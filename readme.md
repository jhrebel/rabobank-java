# Rabobank Java Application

The Rabobank Java Application is an application that validates transactions. It can be run as a standalone application or as an API. 

When run as a standalone application the application will read in the 2 files (records.csv & records.xml) and output the validation to the command line.

When run as an API the application will take as input a file and output the validation for that file

# Building and running the application

Building and running the application is the same in both cases, when in the root directory of the project

Step one:

`mvn clean package`

Step one without running unit tests:

`mvn clean package -Dmaven.test.skip=true`

Step two(on windows):

`java -jar target\rabobank-0.0.1-SNAPSHOT.jar`

Step two (on linux):

`java -jar target/rabobank-0.0.1-SNAPSHOT.jar`


