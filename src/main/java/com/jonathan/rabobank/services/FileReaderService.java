package com.jonathan.rabobank.services;

import com.jonathan.rabobank.util.CSVFileReader;
import com.jonathan.rabobank.util.XMLFileReader;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

@Service
@Log4j2
public class FileReaderService {
    private final CSVFileReader csvFileReader;
    private final XMLFileReader xmlFileReader;

    public FileReaderService(CSVFileReader csvFileReader, XMLFileReader xmlFileReader) {
        this.csvFileReader = csvFileReader;
        this.xmlFileReader = xmlFileReader;
    }

    public List<String[]> readFile(String fileName) {
        String extension = FilenameUtils.getExtension(fileName);

        if(extension.toLowerCase().equals("xml")) {
            return xmlFileReader.readFile(fileName);
        } else if(extension.toLowerCase().equals("csv")) {
            return csvFileReader.readFile(fileName);
        }

        return null;
    }

    public List<String[]> readStream(String fileName, InputStream inputStream) {
        String extension = FilenameUtils.getExtension(fileName);

        if(extension.toLowerCase().equals("xml")) {
            return xmlFileReader.readStream(inputStream);
        } else if(extension.toLowerCase().equals("csv")) {
            return csvFileReader.readStream(inputStream);
        }

        return null;
    }
}
