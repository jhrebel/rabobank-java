package com.jonathan.rabobank.services;

import com.jonathan.rabobank.models.Mutation;
import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.models.enumerator.MutationType;
import com.jonathan.rabobank.util.RecordConstants;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Long.parseLong;

@Service
public class TransactionMapperService {
    /**
     * mapFileOutputToTransactions maps the records from the file to transactions for further processing later on.
     * @param fileOutputRecords
     * @return List<Transaction> transactions
     */
    public List<Transaction> mapFileOutputToTransactions(List<String[]> fileOutputRecords) {
        List<Transaction> transactions = new ArrayList<>();
        for(String[] record: fileOutputRecords) {
            transactions.add(recordToTransaction(record));
        }

        return transactions;
    }

    /**
     * recordToTransaction maps a single record to a transaction
     * @param String[] record
     * @return Transaction transaction
     */
    private Transaction recordToTransaction(String[] record) {
        Transaction transaction = new Transaction();
        transaction.setTransactionReference(parseLong(record[RecordConstants.REFERENCE_NUMBER]));
        transaction.setAccountNumber(record[RecordConstants.ACCOUNT_NUMBER]);
        transaction.setDescription(record[RecordConstants.DESCRIPTION]);
        transaction.setStartBalance(parseDouble(record[RecordConstants.START_BALANCE]));

        Mutation mutation = new Mutation();
        if(record[RecordConstants.MUTATION].charAt(0) == '+') {
            mutation.setMutationType(MutationType.ADDITION);
        } else {
            mutation.setMutationType(MutationType.DEDUCTION);
        }

        //parse everything after the + or - sign as a double
        mutation.setAmount(parseDouble(record[RecordConstants.MUTATION].substring(1)));

        transaction.setMutation(mutation);
        transaction.setEndBalance(parseDouble(record[RecordConstants.END_BALANCE]));

        return transaction;
    }
}
