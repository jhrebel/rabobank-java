package com.jonathan.rabobank.services;

import com.jonathan.rabobank.exceptions.InvalidEndBalanceException;
import com.jonathan.rabobank.exceptions.NonUniqueTransactionException;
import com.jonathan.rabobank.models.InvalidTransaction;
import com.jonathan.rabobank.models.InvalidTransactionReport;
import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.models.enumerator.MutationType;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class TransactionValidationService {
    private final double THRESHOLD = .00001;

    /**
     * validateTransactions loops through the transactions and checks for every transaction if the end balance is
     * correct and if the reference number is unique. Returns a list of all invalid transactions.
     * @param transactionList
     * @return List<InvalidTransaction> invalidTransactionList
     */
    public InvalidTransactionReport validateTransactions(List<Transaction> transactionList) {
        List<InvalidTransaction> invalidTransactionList = new ArrayList<>();
        for(int i = 0; i < transactionList.size(); i++) {
            List<String> errorMessages = new ArrayList<>();
            Transaction transaction = transactionList.get(i);

            try {
                validateEndBalance(transaction);
            } catch (InvalidEndBalanceException exception) {
                errorMessages.add(exception.getMessage());
            }

            try {
                transactionReferenceExists(transactionList, transaction, i);
            } catch (NonUniqueTransactionException exception) {
                errorMessages.add(exception.getMessage());
            }

            if(errorMessages.size() > 0) {
                InvalidTransaction invalidTransaction = new InvalidTransaction();
                invalidTransaction.setTransaction(transaction);
                invalidTransaction.setErrorMessages(errorMessages);
                invalidTransactionList.add(invalidTransaction);
            }
        }

        InvalidTransactionReport invalidTransactionReport = new InvalidTransactionReport();
        invalidTransactionReport.setInvalidTransactionList(invalidTransactionList);

        return invalidTransactionReport;
    }

    /**
     * validateEndBalance validates the end balance by comparing an expected end balance with the provided end balance
     * @param transaction
     * @throws InvalidEndBalanceException
     */
    private void validateEndBalance(Transaction transaction) throws InvalidEndBalanceException{
        double expectedEndBalance;
        if(transaction.getMutation().getMutationType().equals(MutationType.ADDITION)) {
            expectedEndBalance = transaction.getStartBalance() + transaction.getMutation().getAmount();
        } else {
            expectedEndBalance = transaction.getStartBalance() - transaction.getMutation().getAmount();
        }

        if(Math.abs(expectedEndBalance - transaction.getEndBalance()) > THRESHOLD) {
            throw new InvalidEndBalanceException("End balance invalid, expected: " + expectedEndBalance +
                    ", received: " + transaction.getEndBalance());
        }
    }

    /**
     * transactionReferenceExists checks if a transaction with a matching transaction reference can be found in the list
     * up until that list item (for performance reasons we dont loop over the whole list)
     * If a match is found a NonUniqueTransactionException is thrown
     * @param transactionList
     * @param transaction
     * @param transactionIndex
     * @throws NonUniqueTransactionException
     */
    private void transactionReferenceExists(List<Transaction> transactionList, Transaction transaction, Integer transactionIndex) throws NonUniqueTransactionException {
        //no use continuing, first item in the list
        if(transactionIndex == 0) {
            return;
        }

        // only loop until the current element
        for( int i = 0; i < transactionIndex; i++) {
            Transaction transactionToCompare = transactionList.get(i);

            if(transactionToCompare.getTransactionReference().equals(transaction.getTransactionReference())) {
                // indexes start at 0 so a +1 to the transactionIndex
                throw new NonUniqueTransactionException("Duplicate entry: record number "
                        + (transactionIndex+1) + " is duplicate of record number " + (i+1));
            }
        }
    }

}
