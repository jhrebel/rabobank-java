package com.jonathan.rabobank.interfaces;

import java.io.InputStream;
import java.util.List;

public interface FileReaderInterface {
    List<String[]> readFile(String fileName);

    List<String[]> readStream(InputStream inputStream);
}
