package com.jonathan.rabobank.models;

import lombok.Data;

import java.util.List;

@Data
public class InvalidTransactionReport {
    List<InvalidTransaction> invalidTransactionList;

    @Override
    public String toString() {
        StringBuilder outputString = new StringBuilder();
        for(InvalidTransaction invalidTransaction: invalidTransactionList) {
            outputString.append(invalidTransaction.toString());
        }

        return outputString.toString();
    }
}
