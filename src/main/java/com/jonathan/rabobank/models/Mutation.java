package com.jonathan.rabobank.models;

import com.jonathan.rabobank.models.enumerator.MutationType;
import lombok.Data;

@Data
public class Mutation {
    private MutationType mutationType;
    private double amount;
}
