package com.jonathan.rabobank.models.enumerator;

public enum MutationType {
    ADDITION,
    DEDUCTION
}
