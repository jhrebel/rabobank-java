package com.jonathan.rabobank.models;

import lombok.Data;

@Data
public class Transaction {
    private Long transactionReference;
    private String accountNumber;
    private double startBalance;
    private Mutation mutation;
    private String description;
    private double endBalance;
}
