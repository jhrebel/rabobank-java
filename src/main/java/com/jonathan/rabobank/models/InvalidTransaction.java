package com.jonathan.rabobank.models;

import lombok.Data;
import java.util.List;

@Data
public class InvalidTransaction {
    private Transaction transaction;
    private List<String> errorMessages;

    @Override
    public String toString() {
        return "Validation failed for transaction reference: "+transaction.getTransactionReference()+
                " with description: " + transaction.getDescription() + " with the following error(s): " + errorMessages.toString() + "\n";
    }
}
