package com.jonathan.rabobank.exceptions;

import lombok.Data;

@Data
public class IncorrectTransactionException extends Exception {
    IncorrectTransactionException(String message) {
        super(message);
    }
}
