package com.jonathan.rabobank.exceptions;

import lombok.Data;

@Data
public class NonUniqueTransactionException extends IncorrectTransactionException {
    public NonUniqueTransactionException(String message) {
        super(message);
    }
}
