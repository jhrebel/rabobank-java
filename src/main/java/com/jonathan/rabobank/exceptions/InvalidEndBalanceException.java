package com.jonathan.rabobank.exceptions;

import lombok.Data;

@Data
public class InvalidEndBalanceException extends IncorrectTransactionException {
    public InvalidEndBalanceException(String message) {
        super(message);
    }
}
