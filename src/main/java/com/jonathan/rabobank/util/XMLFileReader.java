package com.jonathan.rabobank.util;

import com.jonathan.rabobank.interfaces.FileReaderInterface;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class XMLFileReader implements FileReaderInterface {
    @Override
    public List<String[]> readFile(String fileName) {
        return readStream(getClass().getResourceAsStream(fileName));
    }

    public List<String[]> readStream(InputStream inputStream) {
        List<String[]> records = new ArrayList<>();
        try {
            NodeList nodeList = extractRecordsFromFile(inputStream);

            for(int i = 0; i < nodeList.getLength(); i++) {
                records.add(extractDataFromNode(nodeList.item(i)));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return records;
    }

    private NodeList extractRecordsFromFile(InputStream inputStream) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(inputStream);
        doc.getDocumentElement().normalize();

        return doc.getElementsByTagName("record");
    }

    private String[] extractDataFromNode(Node node) {
        String[] record = new String[RecordConstants.NUMBER_OF_FIELDS];
        NamedNodeMap nodeAttributes = node.getAttributes();
        record[RecordConstants.REFERENCE_NUMBER] = nodeAttributes.getNamedItem("reference").getNodeValue();

        Element element = (Element) node;
        record[RecordConstants.ACCOUNT_NUMBER] = extractPropertyFromElement(element, "accountNumber");
        record[RecordConstants.DESCRIPTION] = extractPropertyFromElement(element, "description");
        record[RecordConstants.START_BALANCE] = extractPropertyFromElement(element, "startBalance");
        record[RecordConstants.MUTATION] = extractPropertyFromElement(element, "mutation");
        record[RecordConstants.END_BALANCE] = extractPropertyFromElement(element, "endBalance");

        return record;
    }

    private String extractPropertyFromElement(Element element, String property) {
        return element.getElementsByTagName(property).item(0).getTextContent();
    }
}
