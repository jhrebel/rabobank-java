package com.jonathan.rabobank.util;

public final class RecordConstants {
    public static final Integer REFERENCE_NUMBER = 0;
    public static final Integer ACCOUNT_NUMBER = 1;
    public static final Integer DESCRIPTION = 2;
    public static final Integer START_BALANCE = 3;
    public static final Integer MUTATION = 4;
    public static final Integer END_BALANCE = 5;

    public static final Integer NUMBER_OF_FIELDS = 6;
}
