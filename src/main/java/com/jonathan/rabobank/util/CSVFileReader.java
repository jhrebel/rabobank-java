package com.jonathan.rabobank.util;

import com.jonathan.rabobank.interfaces.FileReaderInterface;
import com.opencsv.CSVReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class CSVFileReader implements FileReaderInterface {
    @Override
    public List<String[]> readFile(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        return readStream(inputStream);
    }

    public List<String[]> readStream(InputStream inputStream) {
        List<String[]> lines = new ArrayList<>();
        try {
            CSVReader csvReader = new CSVReader(new InputStreamReader(inputStream, "windows-1252"));

            String[] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                lines.add(nextLine);
            }

            // first line contains the column names and the values there can't be parsed by our mapper
            lines.remove(0);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        return lines;
    }
}
