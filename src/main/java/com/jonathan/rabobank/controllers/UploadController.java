package com.jonathan.rabobank.controllers;

import com.jonathan.rabobank.models.InvalidTransactionReport;
import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.services.FileReaderService;
import com.jonathan.rabobank.services.TransactionMapperService;
import com.jonathan.rabobank.services.TransactionValidationService;
import com.jonathan.rabobank.util.CSVFileReader;
import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;

@RestController
@Log4j2
@RequestMapping("/upload")
public class UploadController {
    private final FileReaderService fileReaderService;
    private final TransactionMapperService transactionMapperService;
    private final TransactionValidationService transactionValidationService;

    public UploadController(FileReaderService fileReaderService, TransactionMapperService transactionMapperService, TransactionValidationService transactionValidationService) {
        this.fileReaderService = fileReaderService;
        this.transactionMapperService = transactionMapperService;
        this.transactionValidationService = transactionValidationService;
    }

    @PostMapping("")
    public InvalidTransactionReport uploadFile(@RequestParam MultipartFile file) throws Exception {
        InputStream inputStream = file.getInputStream();
        List<String[]> records = fileReaderService.readStream(file.getOriginalFilename(), inputStream);
        List<Transaction> transactionList = transactionMapperService.mapFileOutputToTransactions(records);
        return transactionValidationService.validateTransactions(transactionList);
    }
}
