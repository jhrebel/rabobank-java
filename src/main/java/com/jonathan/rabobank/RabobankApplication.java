package com.jonathan.rabobank;

import com.jonathan.rabobank.models.InvalidTransaction;
import com.jonathan.rabobank.models.InvalidTransactionReport;
import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.services.FileReaderService;
import com.jonathan.rabobank.services.TransactionMapperService;
import com.jonathan.rabobank.services.TransactionValidationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

@SpringBootApplication
@Log4j2
public class RabobankApplication implements ApplicationRunner {
	private final FileReaderService fileReaderService;
	private final TransactionMapperService transactionMapperService;
	private final TransactionValidationService transactionValidationService;

	public RabobankApplication(FileReaderService fileReaderService, TransactionMapperService transactionMapperService, TransactionValidationService transactionValidationService) {
		this.fileReaderService = fileReaderService;
		this.transactionMapperService = transactionMapperService;
		this.transactionValidationService = transactionValidationService;
	}

	public static void main(String[] args) {
		SpringApplication.run(RabobankApplication.class, args);
	}

	@Override
	public void run (ApplicationArguments args)
	{
		processSingleFile("records.csv");
		processSingleFile("records.xml");
	}

	/**
	 * readAndValidateFiles reads in the transaction files and validates the records
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void processFiles() {

	}

	public void processSingleFile(String fileName) {
		List<String[]> fileOutputRecords = fileReaderService.readFile(fileName);
		List<Transaction> transactionList = transactionMapperService.mapFileOutputToTransactions(fileOutputRecords);

		InvalidTransactionReport invalidTransactionReport = transactionValidationService.validateTransactions(transactionList);
		System.out.println("Invalid records found in "+fileName);
		System.out.println(invalidTransactionReport.toString());

	}

}
