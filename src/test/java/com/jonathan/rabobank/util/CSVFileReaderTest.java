package com.jonathan.rabobank.util;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class CSVFileReaderTest {
    @Autowired
    private CSVFileReader csvFileReader;

    @Test
    public void testCanReadCSVFile() {
        List<String[]> records = csvFileReader.readFile("csvFileReaderUtilTestCsv.csv");

        String[] firstLine = records.get(0);

        assertEquals("12345", firstLine[0]);
        assertEquals("this is a test", firstLine[1]);
        assertEquals("123.4", firstLine[2]);

        String[] secondLine = records.get(1);
        assertEquals("9765", secondLine[0]);
        assertEquals("another ' string", secondLine[1]);
        assertEquals("976.5", secondLine[2]);
    }
}
