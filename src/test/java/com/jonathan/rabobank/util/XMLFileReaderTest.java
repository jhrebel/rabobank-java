package com.jonathan.rabobank.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class XMLFileReaderTest {
    @Autowired
    private XMLFileReader xmlFileReader;

    @Test
    public void testCanReadXMLFile() {
        List<String[]> records = xmlFileReader.readFile("xmlFileReaderUtilTestXml.xml");

        String[] firstLine = records.get(0);

        assertEquals("164702", firstLine[RecordConstants.REFERENCE_NUMBER]);
        assertEquals("NL46ABNA0625805417", firstLine[RecordConstants.ACCOUNT_NUMBER]);
        assertEquals("Flowers for Rik Dekker", firstLine[RecordConstants.DESCRIPTION]);
        assertEquals("81.89", firstLine[RecordConstants.START_BALANCE]);
        assertEquals("+5.99", firstLine[RecordConstants.MUTATION]);
        assertEquals("87.88", firstLine[RecordConstants.END_BALANCE]);

        String[] secondLine = records.get(1);
        assertEquals("189177", secondLine[RecordConstants.REFERENCE_NUMBER]);
        assertEquals("NL27SNSB0917829871", secondLine[RecordConstants.ACCOUNT_NUMBER]);
        assertEquals("Subscription for Erik Dekker", secondLine[RecordConstants.DESCRIPTION]);
        assertEquals("5429", secondLine[RecordConstants.START_BALANCE]);
        assertEquals("-939", secondLine[RecordConstants.MUTATION]);
        assertEquals("6368", secondLine[RecordConstants.END_BALANCE]);
    }
}
