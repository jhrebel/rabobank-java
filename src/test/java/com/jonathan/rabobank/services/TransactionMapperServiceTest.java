package com.jonathan.rabobank.services;

import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.models.enumerator.MutationType;
import com.jonathan.rabobank.util.RecordConstants;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Long.parseLong;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionMapperServiceTest {
    @Autowired
    TransactionMapperService transactionMapperService;

    @Test
    public void testMapping() {
        List<String[]> testRecords = generateTestData();

        List<Transaction> transactions = transactionMapperService.mapFileOutputToTransactions(testRecords);

        Transaction testTransaction = transactions.get(0);
        String[] testRecord = testRecords.get(0);

        assertEquals(parseLong(testRecord[RecordConstants.REFERENCE_NUMBER]), (long) testTransaction.getTransactionReference());
        assertEquals(testRecord[RecordConstants.DESCRIPTION], testTransaction.getDescription());
        assertEquals(testRecord[RecordConstants.ACCOUNT_NUMBER], testTransaction.getAccountNumber());
        assertEquals(parseDouble(testRecord[RecordConstants.START_BALANCE]), testTransaction.getStartBalance(), 0.0);
        assertEquals(MutationType.ADDITION, testTransaction.getMutation().getMutationType());
        assertEquals((double) 10, testTransaction.getMutation().getAmount(), 0.0);
        assertEquals(parseDouble(testRecord[RecordConstants.END_BALANCE]), testTransaction.getEndBalance(), 0.0);

    }

    public List<String[]> generateTestData() {
        List<String[]> testRecords = new ArrayList<>();
        String[] record = new String[RecordConstants.NUMBER_OF_FIELDS];

        record[RecordConstants.REFERENCE_NUMBER] = "2343434";
        record[RecordConstants.DESCRIPTION] = "This is a test description";
        record[RecordConstants.ACCOUNT_NUMBER] = "NL46ABNA232323";
        record[RecordConstants.START_BALANCE] = "100";
        record[RecordConstants.MUTATION] = "+10";
        record[RecordConstants.END_BALANCE] = "110";

        testRecords.add(record);

        return testRecords;
    }
}
