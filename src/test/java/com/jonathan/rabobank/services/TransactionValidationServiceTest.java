package com.jonathan.rabobank.services;

import com.jonathan.rabobank.models.InvalidTransaction;
import com.jonathan.rabobank.models.InvalidTransactionReport;
import com.jonathan.rabobank.models.Mutation;
import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.models.enumerator.MutationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionValidationServiceTest {
    @Autowired
    private TransactionValidationService transactionValidationService;

    @Test
    public void testInvalidTransactionIncorrectEndBalance() {
        List<Transaction> transactionList = new ArrayList<>();
        Transaction testTransaction = generateTransaction(2323424244L, false);

        transactionList.add(testTransaction);

        InvalidTransactionReport report = transactionValidationService.validateTransactions(transactionList);
        List<InvalidTransaction> invalidTransactionList = report.getInvalidTransactionList();

        assertEquals(1, invalidTransactionList.size());

        assertEquals(testTransaction.getTransactionReference(),
                invalidTransactionList.get(0).getTransaction().getTransactionReference());

        assertTrue(invalidTransactionList.get(0).getErrorMessages().get(0)
                .equals("End balance invalid, expected: 889.0, received: 890.0"));
    }

    @Test
    public void testValidTransactionEndBalance() {
        List<Transaction> transactionList = new ArrayList<>();
        Transaction testTransaction = generateTransaction(2323424244L, true);

        transactionList.add(testTransaction);

        InvalidTransactionReport report = transactionValidationService.validateTransactions(transactionList);
        List<InvalidTransaction> invalidTransactionList = report.getInvalidTransactionList();

        assertEquals(0, invalidTransactionList.size());
    }

    @Test
    public void testInvalidTransactionNonUniqueTransaction() {
        List<Transaction> transactionList = new ArrayList<>();

        Transaction testTransaction = generateTransaction(2323424244L, true);
        Transaction testTransactionTwo = generateTransaction(2323424244L, true);

        transactionList.add(testTransaction);
        transactionList.add(testTransactionTwo);

        InvalidTransactionReport report = transactionValidationService.validateTransactions(transactionList);
        List<InvalidTransaction> invalidTransactionList = report.getInvalidTransactionList();

        assertEquals(1, invalidTransactionList.size());

        assertEquals(testTransactionTwo.getTransactionReference(),
                invalidTransactionList.get(0).getTransaction().getTransactionReference());

        assertEquals("Duplicate entry: record number 2 is duplicate of record number 1",
                invalidTransactionList.get(0).getErrorMessages().get(0));
    }

    @Test
    public void testInvalidTransactionNonUniqueAndIncorrectEndBalanceTransaction() {
        List<Transaction> transactionList = new ArrayList<>();

        Transaction testTransaction = generateTransaction(2323424244L, true);
        Transaction testTransactionTwo = generateTransaction(2323424244L, false);

        transactionList.add(testTransaction);
        transactionList.add(testTransactionTwo);

        InvalidTransactionReport report = transactionValidationService.validateTransactions(transactionList);
        List<InvalidTransaction> invalidTransactionList = report.getInvalidTransactionList();

        assertEquals(1, invalidTransactionList.size());

        assertEquals(testTransactionTwo.getTransactionReference(),
                invalidTransactionList.get(0).getTransaction().getTransactionReference());

        assertEquals(2, invalidTransactionList.get(0).getErrorMessages().size());

        assertEquals(invalidTransactionList.get(0).getErrorMessages().get(0),
                "End balance invalid, expected: 889.0, received: 890.0");

        assertEquals("Duplicate entry: record number 2 is duplicate of record number 1",
                invalidTransactionList.get(0).getErrorMessages().get(1));
    }

    /**
     * generateTransaction, easy method to generate some valid and invalid transactions
     * @param transactionReference
     * @param withValidEndBalance
     * @return
     */
    private Transaction generateTransaction(Long transactionReference, boolean withValidEndBalance) {
        Transaction testTransaction = new Transaction();
        testTransaction.setTransactionReference(transactionReference);
        testTransaction.setDescription("This is a test transaction");
        testTransaction.setAccountNumber("NL9INGB3324234");

        if(withValidEndBalance) {
            testTransaction.setStartBalance(900.0);
            Mutation mutation = new Mutation();
            mutation.setMutationType(MutationType.DEDUCTION);
            mutation.setAmount(10.0);
            testTransaction.setMutation(mutation);
            testTransaction.setEndBalance(890.0);
        } else {
            testTransaction.setStartBalance(900.0);
            Mutation mutation = new Mutation();
            mutation.setMutationType(MutationType.DEDUCTION);
            mutation.setAmount(11.0);
            testTransaction.setMutation(mutation);
            testTransaction.setEndBalance(890.0);
        }

        return testTransaction;
    }
}
