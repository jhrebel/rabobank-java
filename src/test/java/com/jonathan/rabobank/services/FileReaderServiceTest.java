package com.jonathan.rabobank.services;

import com.jonathan.rabobank.models.Transaction;
import com.jonathan.rabobank.util.CSVFileReader;
import com.jonathan.rabobank.util.XMLFileReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileReaderServiceTest {
    @Autowired
    private FileReaderService fileReaderService;

    @MockBean
    CSVFileReader csvFileReader;

    @MockBean
    XMLFileReader xmlFileReader;

    @Test
    public void testCallsCsvFileReader() {
        fileReaderService.readFile("fileReaderServiceTestCsv.csv");
        verify(csvFileReader, atLeast(1)).readFile(any());
    }

    @Test
    public void testCallsXMLFileReader() {
        fileReaderService.readFile("fileReaderServiceTestXml.xml");
        verify(xmlFileReader, atLeast(1)).readFile(any());
    }
}
